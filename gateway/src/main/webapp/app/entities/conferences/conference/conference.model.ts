import * as dayjs from 'dayjs';

export interface IConference {
  id?: number;
  name?: string | null;
  participants?: number | null;
  date?: dayjs.Dayjs | null;
}

export class Conference implements IConference {
  constructor(public id?: number, public name?: string | null, public participants?: number | null, public date?: dayjs.Dayjs | null) {}
}

export function getConferenceIdentifier(conference: IConference): number | undefined {
  return conference.id;
}
