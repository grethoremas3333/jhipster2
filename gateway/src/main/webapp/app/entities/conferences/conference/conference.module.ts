import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ConferenceComponent } from './list/conference.component';
import { ConferenceDetailComponent } from './detail/conference-detail.component';
import { ConferenceUpdateComponent } from './update/conference-update.component';
import { ConferenceDeleteDialogComponent } from './delete/conference-delete-dialog.component';
import { ConferenceRoutingModule } from './route/conference-routing.module';

@NgModule({
  imports: [SharedModule, ConferenceRoutingModule],
  declarations: [ConferenceComponent, ConferenceDetailComponent, ConferenceUpdateComponent, ConferenceDeleteDialogComponent],
  entryComponents: [ConferenceDeleteDialogComponent],
})
export class ConferencesConferenceModule {}
