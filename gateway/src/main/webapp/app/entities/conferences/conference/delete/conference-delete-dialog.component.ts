import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IConference } from '../conference.model';
import { ConferenceService } from '../service/conference.service';

@Component({
  templateUrl: './conference-delete-dialog.component.html',
})
export class ConferenceDeleteDialogComponent {
  conference?: IConference;

  constructor(protected conferenceService: ConferenceService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.conferenceService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
