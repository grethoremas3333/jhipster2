import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ConferenceDetailComponent } from './conference-detail.component';

describe('Conference Management Detail Component', () => {
  let comp: ConferenceDetailComponent;
  let fixture: ComponentFixture<ConferenceDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ConferenceDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ conference: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ConferenceDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ConferenceDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load conference on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.conference).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
