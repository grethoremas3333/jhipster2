import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IConference } from '../conference.model';

@Component({
  selector: 'jhi-conference-detail',
  templateUrl: './conference-detail.component.html',
})
export class ConferenceDetailComponent implements OnInit {
  conference: IConference | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ conference }) => {
      this.conference = conference;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
