import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ConferenceService } from '../service/conference.service';

import { ConferenceComponent } from './conference.component';

describe('Conference Management Component', () => {
  let comp: ConferenceComponent;
  let fixture: ComponentFixture<ConferenceComponent>;
  let service: ConferenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ConferenceComponent],
    })
      .overrideTemplate(ConferenceComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConferenceComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ConferenceService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.conferences?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
