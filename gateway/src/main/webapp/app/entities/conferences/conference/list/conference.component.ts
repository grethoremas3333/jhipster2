import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IConference } from '../conference.model';
import { ConferenceService } from '../service/conference.service';
import { ConferenceDeleteDialogComponent } from '../delete/conference-delete-dialog.component';

@Component({
  selector: 'jhi-conference',
  templateUrl: './conference.component.html',
})
export class ConferenceComponent implements OnInit {
  conferences?: IConference[];
  isLoading = false;

  constructor(protected conferenceService: ConferenceService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.conferenceService.query().subscribe(
      (res: HttpResponse<IConference[]>) => {
        this.isLoading = false;
        this.conferences = res.body ?? [];
      },
      () => {
        this.isLoading = false;
      }
    );
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IConference): number {
    return item.id!;
  }

  delete(conference: IConference): void {
    const modalRef = this.modalService.open(ConferenceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.conference = conference;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
