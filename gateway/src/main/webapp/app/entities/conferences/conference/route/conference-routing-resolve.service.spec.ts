jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IConference, Conference } from '../conference.model';
import { ConferenceService } from '../service/conference.service';

import { ConferenceRoutingResolveService } from './conference-routing-resolve.service';

describe('Conference routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: ConferenceRoutingResolveService;
  let service: ConferenceService;
  let resultConference: IConference | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(ConferenceRoutingResolveService);
    service = TestBed.inject(ConferenceService);
    resultConference = undefined;
  });

  describe('resolve', () => {
    it('should return IConference returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultConference = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultConference).toEqual({ id: 123 });
    });

    it('should return new IConference if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultConference = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultConference).toEqual(new Conference());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Conference })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultConference = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultConference).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
