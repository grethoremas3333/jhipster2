import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IConference, Conference } from '../conference.model';
import { ConferenceService } from '../service/conference.service';

@Injectable({ providedIn: 'root' })
export class ConferenceRoutingResolveService implements Resolve<IConference> {
  constructor(protected service: ConferenceService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IConference> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((conference: HttpResponse<Conference>) => {
          if (conference.body) {
            return of(conference.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Conference());
  }
}
