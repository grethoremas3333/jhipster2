import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ConferenceComponent } from '../list/conference.component';
import { ConferenceDetailComponent } from '../detail/conference-detail.component';
import { ConferenceUpdateComponent } from '../update/conference-update.component';
import { ConferenceRoutingResolveService } from './conference-routing-resolve.service';

const conferenceRoute: Routes = [
  {
    path: '',
    component: ConferenceComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ConferenceDetailComponent,
    resolve: {
      conference: ConferenceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ConferenceUpdateComponent,
    resolve: {
      conference: ConferenceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ConferenceUpdateComponent,
    resolve: {
      conference: ConferenceRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(conferenceRoute)],
  exports: [RouterModule],
})
export class ConferenceRoutingModule {}
