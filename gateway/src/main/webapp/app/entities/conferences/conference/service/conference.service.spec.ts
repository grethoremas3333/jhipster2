import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IConference, Conference } from '../conference.model';

import { ConferenceService } from './conference.service';

describe('Conference Service', () => {
  let service: ConferenceService;
  let httpMock: HttpTestingController;
  let elemDefault: IConference;
  let expectedResult: IConference | IConference[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ConferenceService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      name: 'AAAAAAA',
      participants: 0,
      date: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Conference', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.create(new Conference()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Conference', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          participants: 1,
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Conference', () => {
      const patchObject = Object.assign(
        {
          participants: 1,
        },
        new Conference()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Conference', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          name: 'BBBBBB',
          participants: 1,
          date: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          date: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Conference', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addConferenceToCollectionIfMissing', () => {
      it('should add a Conference to an empty array', () => {
        const conference: IConference = { id: 123 };
        expectedResult = service.addConferenceToCollectionIfMissing([], conference);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(conference);
      });

      it('should not add a Conference to an array that contains it', () => {
        const conference: IConference = { id: 123 };
        const conferenceCollection: IConference[] = [
          {
            ...conference,
          },
          { id: 456 },
        ];
        expectedResult = service.addConferenceToCollectionIfMissing(conferenceCollection, conference);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Conference to an array that doesn't contain it", () => {
        const conference: IConference = { id: 123 };
        const conferenceCollection: IConference[] = [{ id: 456 }];
        expectedResult = service.addConferenceToCollectionIfMissing(conferenceCollection, conference);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(conference);
      });

      it('should add only unique Conference to an array', () => {
        const conferenceArray: IConference[] = [{ id: 123 }, { id: 456 }, { id: 83226 }];
        const conferenceCollection: IConference[] = [{ id: 123 }];
        expectedResult = service.addConferenceToCollectionIfMissing(conferenceCollection, ...conferenceArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const conference: IConference = { id: 123 };
        const conference2: IConference = { id: 456 };
        expectedResult = service.addConferenceToCollectionIfMissing([], conference, conference2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(conference);
        expect(expectedResult).toContain(conference2);
      });

      it('should accept null and undefined values', () => {
        const conference: IConference = { id: 123 };
        expectedResult = service.addConferenceToCollectionIfMissing([], null, conference, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(conference);
      });

      it('should return initial array if no Conference is added', () => {
        const conferenceCollection: IConference[] = [{ id: 123 }];
        expectedResult = service.addConferenceToCollectionIfMissing(conferenceCollection, undefined, null);
        expect(expectedResult).toEqual(conferenceCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
