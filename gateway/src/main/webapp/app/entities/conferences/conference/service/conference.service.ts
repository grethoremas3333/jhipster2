import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IConference, getConferenceIdentifier } from '../conference.model';

export type EntityResponseType = HttpResponse<IConference>;
export type EntityArrayResponseType = HttpResponse<IConference[]>;

@Injectable({ providedIn: 'root' })
export class ConferenceService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/conferences', 'conferences');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(conference: IConference): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(conference);
    return this.http
      .post<IConference>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(conference: IConference): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(conference);
    return this.http
      .put<IConference>(`${this.resourceUrl}/${getConferenceIdentifier(conference) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(conference: IConference): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(conference);
    return this.http
      .patch<IConference>(`${this.resourceUrl}/${getConferenceIdentifier(conference) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IConference>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IConference[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addConferenceToCollectionIfMissing(
    conferenceCollection: IConference[],
    ...conferencesToCheck: (IConference | null | undefined)[]
  ): IConference[] {
    const conferences: IConference[] = conferencesToCheck.filter(isPresent);
    if (conferences.length > 0) {
      const conferenceCollectionIdentifiers = conferenceCollection.map(conferenceItem => getConferenceIdentifier(conferenceItem)!);
      const conferencesToAdd = conferences.filter(conferenceItem => {
        const conferenceIdentifier = getConferenceIdentifier(conferenceItem);
        if (conferenceIdentifier == null || conferenceCollectionIdentifiers.includes(conferenceIdentifier)) {
          return false;
        }
        conferenceCollectionIdentifiers.push(conferenceIdentifier);
        return true;
      });
      return [...conferencesToAdd, ...conferenceCollection];
    }
    return conferenceCollection;
  }

  protected convertDateFromClient(conference: IConference): IConference {
    return Object.assign({}, conference, {
      date: conference.date?.isValid() ? conference.date.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.date = res.body.date ? dayjs(res.body.date) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((conference: IConference) => {
        conference.date = conference.date ? dayjs(conference.date) : undefined;
      });
    }
    return res;
  }
}
