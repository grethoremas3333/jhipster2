jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ConferenceService } from '../service/conference.service';
import { IConference, Conference } from '../conference.model';

import { ConferenceUpdateComponent } from './conference-update.component';

describe('Conference Management Update Component', () => {
  let comp: ConferenceUpdateComponent;
  let fixture: ComponentFixture<ConferenceUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let conferenceService: ConferenceService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ConferenceUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(ConferenceUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ConferenceUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    conferenceService = TestBed.inject(ConferenceService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const conference: IConference = { id: 456 };

      activatedRoute.data = of({ conference });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(conference));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Conference>>();
      const conference = { id: 123 };
      jest.spyOn(conferenceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conference });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: conference }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(conferenceService.update).toHaveBeenCalledWith(conference);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Conference>>();
      const conference = new Conference();
      jest.spyOn(conferenceService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conference });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: conference }));
      saveSubject.complete();

      // THEN
      expect(conferenceService.create).toHaveBeenCalledWith(conference);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Conference>>();
      const conference = { id: 123 };
      jest.spyOn(conferenceService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ conference });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(conferenceService.update).toHaveBeenCalledWith(conference);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
