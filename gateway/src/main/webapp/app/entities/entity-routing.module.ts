import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'conference',
        data: { pageTitle: 'Conferences' },
        loadChildren: () => import('./conferences/conference/conference.module').then(m => m.ConferencesConferenceModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
